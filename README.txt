Implementation of Ruby enumerable module for PHP.

1. Coverage
Enumerable
  all
  any
  cycle
  reduce/inject
  reject
  take
  partition
  eachSlice
  eachWithIndex

Enumerator
  range

2. Docs
- http://ruby-doc.org/core-2.3.0/Enumerable.html
- http://ruby-doc.org/core-2.2.0/Enumerator.html
