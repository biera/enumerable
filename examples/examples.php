<?php

require dirname(__DIR__) . DIRECTORY_SEPARATOR . 'vendor/autoload.php';

use Enumerable\Enumerable, Enumerable\Enumerator;

/**
 * Example 1.
 */
function fibonacciNumbersGenerator()
{
   $a = $b = 1;
   while (true) {
       yield $current = $a;
       $a = $b;
       $b = $b + $current;
   }
}

$enumerator = new Enumerator(fibonacciNumbersGenerator());

// get first 10 fibonacci numbers
var_dump($enumerator->take(10) === [1, 1, 2, 3, 5, 8, 13, 21, 34, 55]); // true

/**
 * Example II : Enumerable::each
 */
//$enumerator = new Enumerator([1,2,3,4,5]);
//
//// with callback
//$enumerator->each(function($element) {
//    echo $element, "\n";
//});
//
//// without callback
//foreach($enumerator->each() as $element) {
//    echo $element, "\n";
//}
//
//// but, this is equvalent to:
//foreach($enumerator as $element) {
//    echo $element, "\n";
//}

/**
 * Example III : Enumerable::all
 */
//$enumerable = new Enumerator([true, 1.0, 'true', 1, [1]]);
//
//$isInt = function($item) {
//    return is_int($item);
//};
//
//var_dump(
//    $enumerable->all(),
//    $enumerable->all($isInt),
//    $enumerable->any(),
//    $enumerable->any($isInt)
//);

/**
 * Example IV : Enumerable::eachWithIndex
 */
//$enumerable = new Enumerator(['cat', 'dog', 'cow']);
//
//$enumerable->eachWithIndex(function($item, $index) {
//    echo sprintf("%d => %s\n", $index, $item);
//});


/**
 * Example V: Enumerable::inject
 */
//$enumerable = new Enumerator([1,2,3,4,5]);
//
//var_dump(
//    $enumerable->inject(function($sum, $item) { return $sum + $item; }, 0),
//    $enumerable->inject(function($sum, $item) { return $sum * $item; }, 1)
//);
