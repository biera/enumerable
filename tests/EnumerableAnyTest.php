<?php

use Enumerable\Enumerator;

/**
 * Unit tests for Enumerable::any()
 *
 * @author Jakub Biernacki <kubiernacki@gmail.com>
 */
class EnumerableAnyTest extends \PHPUnit_Framework_TestCase
{
  /**
   * @test
   * @dataProvider dataProvider_withoutCallable
   */
  public function anyTest_withoutCallable($data, $result)
  {
    $this->assertEquals(
      (new Enumerator($data))->any(), $result
    );
  }

  /**
   * @test
   * @dataProvider dataProvider_withCallable
   */
  public function anyTest_withCallable($data, $callable, $result)
  {
    $this->assertEquals(
      (new Enumerator($data))->any($callable), $result
    );
  }

  public function dataProvider_withoutCallable()
  {
    return [
      [[false, null, 1], true],
      [[0, null, []], false]
    ];
  }

  public function dataProvider_withCallable()
  {
    $thereIsAtLeastOneInteger = [
      [11.0, '2', 99.999, []],
      'is_int',
      false
    ];

    $thereIsAtLeastOneOddNumber = [
      [-4, -2, 0, 9, 12],
      function ($param) {
        return is_integer($param) && $param % 2 == 1;
      },
      true
    ];

    return [$thereIsAtLeastOneInteger, $thereIsAtLeastOneOddNumber];
  }
}
