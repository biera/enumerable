<?php

use Enumerable\Enumerator, Enumerable\Enumerable;

/**
 * Unit tests for EnumerableEnuberable::eachSlice
 *
 * @author Jakub Biernacki <kubiernacki@gmail.com>
 */
class EnumerableEachSliceTest extends \PHPUnit_Framework_TestCase
{
  /**
   * Slice size should be noting else but integer
   *
   * @test
   * @expectedException \InvalidArgumentException
   */
  public function invalidArgumentExceptionIsThrown_whenNonIntegerArgumentPassed()
  {
    (new Enumerator())->eachSlice([]);
  }

  /**
   * Slice size should be noting else but positive integer
   *
   * @test
   * @expectedException \InvalidArgumentException
   */
  public function invalidArgumentExceptionIsThrown_whenNonPositiveIntegerPassed()
  {
    (new Enumerator())->eachSlice(0);
  }

  /**
   * @test
   * @dataProvider dataProvider
   */
  public function collectionShouldBeSlicedAccordingToPassedArgument($collection, $collectionSize, $sliceSize, $expectedCountOfSlices)
  {
    $enumerator = new Enumerator($collection);
    $slicedCollection = $enumerator->eachSlice($sliceSize);

    $sliceCounter = 0;
    foreach ($slicedCollection as $slice) {
      $actualSliceSize = count($slice);

      if ($sliceCounter == ($expectedCountOfSlices - 1) && ($reminder = $collectionSize % $sliceSize) > 0) {
        $this->assertEquals($reminder, $actualSliceSize);
      } else {
        $this->assertEquals($sliceSize, $actualSliceSize);
      }

      $sliceCounter++;
    }

    $this->assertEquals($expectedCountOfSlices, $sliceCounter);
  }

  /**
   * @test
   * @dataProvider dataProvider
   */
  public function collectionShouldBeSlicedAccordingToPassedArgument_andCallbackShouldBeExecutedForEachSlice($collection, $collectionSize, $sliceSize, $expectedCountOfSlices)
  {
    $enumerableCollection = new Enumerator($collection);
    $callbackCallCounter = 0;

    $null = $enumerableCollection->eachSlice($sliceSize, function($slice) use (&$callbackCallCounter, $sliceSize, $expectedCountOfSlices, $collectionSize) {
      $actualSliceSize = count($slice);

      if ($callbackCallCounter == ($expectedCountOfSlices - 1) && ($reminder = $collectionSize % $sliceSize) > 0) {
        $this->assertEquals($reminder, $actualSliceSize);
      } else {
        $this->assertEquals($sliceSize, $actualSliceSize);
      }

      $callbackCallCounter++;
    });

    $this->assertEquals($expectedCountOfSlices, $callbackCallCounter);
    $this->assertNull($null);
  }

  public function dataProvider()
  {
    $collection     = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    $collectionSize = count($collection);

    $data = [];
    foreach ([1,3,10] as $silceSize) {
      $data[] = [$collection, $collectionSize, $silceSize, ceil($collectionSize / $silceSize)];
    }

    return $data;
  }
}
