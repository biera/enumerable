<?php

use Enumerable\Enumerator, Enumerable\Enumerable;

/**
 * Unit tests for Biera\Enumerator::__construct
 *
 * Either \Traversable or array (default) are acceptable as constructor parameter,
 * any other throws \InvalidArgumentException
 *
 * @covers Biera\Enumerator::__construct
 */
class EnumeratorInstantiationTest extends \PHPUnit_Framework_TestCase
{
  /**
   * @test
   */
  public function instantiateEnumerator_whenValidParameterIsPassed()
  {
    // no parameter is required
    $this->assertInstanceOf('Enumerable\Enumerator', new Enumerator());

    // array is allowed
    $this->assertInstanceOf('Enumerable\Enumerator', new Enumerator([]));

    // any other \Traversable is also fine
    $this->assertInstanceOf('Enumerable\Enumerator', new Enumerator(new \ArrayObject()));

    // \Generator implements \Traversable
    $trueGenerator = function() { yield true; };
    $this->assertInstanceOf('Enumerable\Enumerator', new Enumerator($trueGenerator()));
  }

  /**
   * @test
   * @expectedException \InvalidArgumentException
   */
  public function invalidArgumentExceptionIsThrown_whenInvalidArgumentIsPassed()
  {
    // string is not valid parameter
    new Enumerator('');
  }
}
