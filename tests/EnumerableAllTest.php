<?php

use Enumerable\Enumerator;

/**
 * Unit tests for Enumerable::all()
 *
 * @author Jakub Biernacki <kubiernacki@gmail.com>
 */
class EnumerableAllTest extends \PHPUnit_Framework_TestCase
{
  /**
   * @test
   * @dataProvider dataProvider_withoutCallable
   */
  public function allTest_withoutCallable($data, $result)
  {
    $this->assertEquals(
      (new Enumerator($data))->all(), $result
    );
  }

  /**
   * @test
   * @dataProvider dataProvider_withCallable
   */
  public function allTest_withCallable($data, $callable, $result)
  {
    $this->assertEquals(
      (new Enumerator($data))->all($callable), $result
    );

    $this->assertEquals(
      (new SupperArray($data))->all($callable), $result
    );
  }

  public function dataProvider_withoutCallable()
  {
    return [
      [[1, true, new stdClass], true],
      [[0, true, []], false]
    ];
  }

  public function dataProvider_withCallable()
  {
    $allIntegers = [
      [1, 2, 99.999, -1],
      'is_int',
      false
    ];

    $allEven = [
      [-4, -2, 0, 10, 12],
      function ($param) {
        return is_integer($param) && $param % 2 == 0;
      },
      true
    ];

    return [$allIntegers, $allEven];
  }
}
