<?php
require dirname(__DIR__) . DIRECTORY_SEPARATOR . 'vendor/autoload.php';

use Enumerable\Enumerable;

class SupperArray extends \ArrayObject
{
  use Enumerable;
}
