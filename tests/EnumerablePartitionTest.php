<?php

use Enumerable\Enumerator, Enumerable\Enumerable;

/**
 * Unit tests for Enumerable::partition
 *
 * @author Jakub Biernacki <kubiernacki@gmail.com>
 *
 * @covers Biera\Enumerable::partition
 */
class EnumerablePartitionTest extends \PHPUnit_Framework_TestCase
{
  /**
   * @test
   */
  public function collectionShouldBePartitioned()
  {
    $collection = [-1, -2, -3, 4, -5];

    // positive | negative
    list($positive, $negative) = (new Enumerator($collection))->partition(function ($item) { return $item >= 0; });
    $this->assertAllPostitive($positive);
    $this->assertAllNegative($negative);

    // even | odd
    list($even, $odd) = (new Enumerator($collection))->partition(function ($item) { return $item % 2 == 0; });
    $this->assertAllEven($even);
    $this->assertAllOdd($odd);

    // true | false
    list($true, $false) = (new Enumerator([1.0, true, null, 0, [], false, new \stdClass]))->partition();
    $this->assertTrue((new Enumerator($true))->all());
    $this->assertFalse((new Enumerator($false))->any());
  }

  private function assertAllEven($collection)
  {
    return $this->assertTrue(
      (new Enumerator($collection))->all(function($item) { return $item % 2 == 0; })
    );
  }

  private function assertAllOdd($collection)
  {
    return $this->assertTrue(
      (new Enumerator($collection))->all(function($item) { return abs($item) % 2 == 1; })
    );
  }

  private function assertAllPostitive($collection)
  {
    return $this->assertTrue(
      (new Enumerator($collection))->all(function($item) { return $item >= 0; })
    );
  }

  private function assertAllNegative($collection)
  {
    return $this->assertTrue(
      (new Enumerator($collection))->all(function($item) { return $item < 0; })
    );
  }
}
