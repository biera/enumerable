<?php

use Enumerable\Enumerator, Enumerable\Enumerable;

/**
 * Unit tests for Enumerable::cycle
 *
 * TODO : How to test infinite generators?
 *
 * @author Jakub Biernacki <kubiernacki@gmail.com>
 *
 * @covers Biera\Enumerable::cycle
 */
class EnumerableCycleTest extends \PHPUnit_Framework_TestCase
{
  const TIMES = 3;

  /**
   * Times parameter should be noting else but integer
   *
   * @test
   * @expectedException \InvalidArgumentException
   */
  public function invalidArgumentExceptionIsThrown_whenNonIntegerArgumentPassed()
  {
    (new Enumerator())->cycle([]);
  }

  /**
   * Times parameter should be positive integer
   *
   * @test
   * @expectedException \InvalidArgumentException
   */
  public function invalidArgumentExceptionIsThrown_whenNonPositiveIntegerPassed()
  {
    (new Enumerator())->cycle(0);
  }

  /**
   * @test
   * @dataProvider dataProvider
   */
  public function a($collection)
  {
    $cycleGenerator = (new Enumerator($collection))->cycle(self::TIMES);

    $iterationCounter = 0;
    while($cycleGenerator->valid()) {
      $cycleGenerator->next();
      $iterationCounter++;
    }

    $this->assertEquals(self::TIMES * count($collection), $iterationCounter);
  }

  public function dataProvider()
  {
    return [
      [[]],
      [[1, 2, 3]]
    ];
  }
}
