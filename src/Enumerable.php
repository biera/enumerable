<?php

namespace Enumerable;

/**
 * Enumerable
 *
 * @author Jakub Biernacki <kubiernacki@gmail.com>
 */
trait Enumerable
{
  /**
   * Passes each element of the collection to the given callable.
   * The method returns true if the callable never returns falsy value.
   * If the callable is not given, implicit one is passed which
   * returns boolean value of passed parameter.
   *
   * @param callable $callable
   *
   * @return boolean
   */
  public function all(callable $callable = null)
  {
    if (!$callable) {
      $callable = function($item) {
        return (boolean) $item;
      };
    }

    foreach ($this as $item) {
      if (!call_user_func($callable, $item)) {
        return false;
      }
    }

    return true;
  }

  /**
   * @param callable $callable
   *
   * @return boolean
   */
  public function any(callable $callable = null)
  {
    if (!$callable) {
      $callable = function($item) {
        return (boolean) $item;
      };
    }

    foreach ($this as $item) {
      if (call_user_func($callable, $item)) {
        return true;
      }
    }

    return false;
  }

  /**
   * @param integer|null $times
   * @param callable     $callable
   *
   * @return \Traversable|null
   */
  public function cycle($times = null, callable $callable = null)
  {
    if (!is_null($times) && (!is_int($times) || $times <= 0)) {
      throw new \InvalidArgumentException(sprintf('Times argument should be a positive integer, %s given', gettype($times)));
    }

    $cycleGenerator = $this->cycleGenerator($this, $times);

    if (!$callable) {
      return $cycleGenerator;
    }

    foreach ($cycleGenerator as $item) {
      call_user_func($callable, $item);
    }
  }

  /**
   * Alias for reduce
   *
   * @param callable $callable
   * @param mixed    $initial
   *
   * @return mixed
   */
  public function inject(callable $callable, $initial)
  {
    return $this->reduce($callable, $initial);
  }

  /**
   * @param callable $callable
   * @param int      $start
   *
   * @return \Traversable|null
   */
  public function eachWithIndex(callable $callable = null, $start = 0)
  {
    $generator = $this->eachWithIndexGenerator($this->getIterator(), $start);

    if (!$callable) {
      return $generator;
    }

    foreach($generator as $item) {
      call_user_func_array($callable, $item);
    }
  }

  /**
   * @param int      $sliceSize
   * @param callable $callable
   *
   * @return Enumerator
   */
  public function eachSlice($sliceSize, callable $callable = null)
  {
    if (!is_int($sliceSize) || $sliceSize <= 0) {
      throw new \InvalidArgumentException('Slice size should be a positive integer.');
    }

    $generator = $this->eachSliceGenerator($this->getIterator(), $sliceSize);

    if (!$callable) {
      return new Enumerator($generator);
    }

    foreach ($generator as $item) {
      call_user_func($callable, $item);
    }
  }

  /**
   * @param callable|null $callable
   *
   * @return array
   */
  public function partition(callable $callable = null)
  {
    if (!$callable) {
      $callable = function($item) {
        return (boolean) $item;
      };
    }

    $a = $b = [];
    foreach ($this as $item) {
      if (call_user_func($callable, $item)) {
        $a[] = $item;
      } else {
        $b[] = $item;
      }
    }

    return [$a, $b];
  }

  /**
   * @param callable $callable
   * @param mixed    $initial
   *
   * @return mixed
   */
  public function reduce(callable $callable, $initial)
  {
    $carry = $initial;

    foreach ($this as $item) {
      $carry = call_user_func_array($callable, [$carry, $item]);
    }

    return $carry;
  }

  /**
   * @param callable $callable
   *
   * @return \Traversable|array
   */
  public function reject(callable $callable)
  {
    $items = $this->createInstance();

    foreach ($this as $item) {
      if (!call_user_func($callable, $item)) {
        $items[] = $item;
      }
    }

    return $items;
  }

  /**
   * Take $size first elements
   *
   * @param int $size
   *
   * @throws \InvalidArgumentException
   *
   * @return array
   */
  public function take($size)
  {
    if (!is_int($size) || $size < 0) {
      throw new \InvalidArgumentException('Size should be a non-negative integer.');
    }

    $iterator = $this->getIterator();
    $items = [];

    while($iterator->valid() && $size-- > 0) {
      $items[] = $iterator->current();
      $iterator->next();
    }

    return $items;
  }

  protected function createInstance()
  {
    static $constructable = null;
    static $selfConstructable = false;

    if (is_null($constructable)) {
      $reflection = new \ReflectionClass($this);

      if (!$reflection->implementsInterface('ArrayAccess')) {
        $constructable = false;
        return [];
      }

      $selfConstructable = $reflection->implementsInterface('Biera\SelfConstructable');

      if (!$selfConstructable && (0 != $reflection->getConstructor()->getNumberOfRequiredParameters())) {
        $constructable = false;
        return [];
      }

      $constructable = true;
    }

    if ($constructable) {
      if ($selfConstructable) {
        return $this::constructItself();
      }

      return new $this;
    }

    return [];
  }

  private function cycleGenerator(\Traversable $iterator, $times = null)
  {
    if (!$times) {
      $times = true;
    }

    while ($times) {
      foreach ($iterator as $item) {
        yield $item;
      }

      if (true !== $times) {
        $times--;
      }

      $iterator->rewind();
    }
  }

  private function eachWithIndexGenerator(\Traversable $iterator, $index)
  {
    foreach ($iterator as $item) {
      yield [$item, $index++];
    }
  }

  private function eachSliceGenerator(\Traversable $iterator, $sliceSize)
  {
    $counter = 0;
    while($iterator->valid())  {

      if ($counter % $sliceSize == 0) {
        if ($counter != 0) {
          yield $slice;
        }
        $slice = $this->createInstance();
      }

      $slice[] = $iterator->current();
      $iterator->next();
      $counter++;
    }

    // yield last slice
    if (!empty($slice)) {
      yield $slice;
    }
  }
}
