<?php

namespace Biera;

interface SelfConstructable
{
    /**
     * Construct itself
     *
     * @return SelfConstructable
     */
    public static function constructItself();
}
