<?php

namespace Enumerable;

class Enumerator implements \IteratorAggregate
{
    use Enumerable;

    /**
     * @var \Traversable
     */
    protected $traversable;

    /**
     * @var \Iterator
     */
    protected $iterator;

    /**
     * @var \Traversable|array
     */
    protected $wrapped;

    public function __construct($traversable = [])
    {
        if (!($isArray = is_array($traversable)) && !($traversable instanceof \Traversable)) {
            throw new \InvalidArgumentException('Parameter should implement \Traversable interface or be an array');
        }

        $this->wrapped = $traversable;

        $this->traversable = $isArray ? new \ArrayObject($traversable) : $traversable;

        $this->iterator = $this->traversable instanceof \IteratorAggregate ? $this->traversable->getIterator() : $this->traversable;
    }

    /**
     * Range generator
     *
     * @param numeric $a
     * @param numeric $b
     * @param int     $step
     *
     * @return \Traversable
     */
    public static function range($a, $b = null, $step = 1)
    {
        $args = func_get_args();

        if (count($args) == 1) {
            $lower = 0;
            $upper = $a;
        } else {
            list($lower, $upper) = $args;
        }

        return self::rangeGenerator($lower, $upper, $step);
    }

    private static function rangeGenerator($lower, $upper, $step)
    {
        if (!is_numeric($lower) || !is_numeric($upper) || !is_numeric($step) ) {
            throw new \InvalidArgumentException('All params should be numeric! Given $lower %s, $upper %s, $step %s', gettype($lower), gettype($upper), gettype($step));
        }

        if ($lower >= $upper) {
            throw new \InvalidArgumentException('Lower bound should be lesser than upper bound!');
        }

        for ($current = $lower; $current <= $upper; $current = $current + $step) {
            yield $current;
        }
    }

    public function getWrapped()
    {
        return $this->wrapped;
    }

    public function next()
    {
        $this->iterator->next();
    }

    public function current()
    {
        return $this->iterator->current();
    }

    public function valid()
    {
        return $this->iterator->valid();
    }

    public function key()
    {
        return $this->iterator->key();
    }

    public function rewind()
    {
        $this->iterator->rewind();
    }

    public function getIterator()
    {
        return $this->iterator;
    }
}
